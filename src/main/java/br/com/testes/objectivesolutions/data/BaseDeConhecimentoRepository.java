package br.com.testes.objectivesolutions.data;

import br.com.testes.objectivesolutions.model.Prato;
import br.com.testes.objectivesolutions.model.TiposDePrato;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BaseDeConhecimentoRepository {


    private Prato pratoPalpite;
    private List<TiposDePrato> baseDeConhecimento;

    public BaseDeConhecimentoRepository() {
        this.baseDeConhecimento = criarPratosIniciais();
        this.criaChuteFinal();
    }

    private static List<TiposDePrato> criarPratosIniciais() {
        Prato lasanha = new Prato("lasanha");
        Prato macarronada = new Prato("macarronada");

        TiposDePrato massa = new TiposDePrato(null, "massa", lasanha);
        massa.addPrato(macarronada);

        List<TiposDePrato> tipos = new ArrayList<>();
        tipos.add(massa);

        return tipos;
    }

    public List<TiposDePrato> getBaseDeConhecimento() {
        return baseDeConhecimento;
    }

    public Prato getPratoPalpite() {
        return pratoPalpite;
    }

    public void escolheUmPratoBaseadoNoTipo(TiposDePrato tipoSelecionado) {
        if (tipoSelecionado != null) {
            pratoPalpite = tipoSelecionado.getPratos().get(new Random().nextInt(tipoSelecionado.getPratos().size()));
        }
    }

    public void criaChuteFinal() {
        this.pratoPalpite = new Prato("Bolo de chocolate");
    }

    public void adicionaOPratoABaseDeConhecimento(TiposDePrato tipoSelecionado, String nomePrato, String nomeCategoria) {
        if (tipoSelecionado == null) {
            baseDeConhecimento.add(new TiposDePrato(null, nomeCategoria, new Prato(nomePrato)));
            return;
        }

        if (tipoSelecionado.getNomeTipo().equalsIgnoreCase(nomeCategoria)) {
            adicionaPratoAoTipoDePrato(tipoSelecionado, nomePrato);
            return;
        }

        tipoSelecionado.addSubTipo(new TiposDePrato(null, nomeCategoria, new Prato(nomePrato)));
    }

    private void adicionaPratoAoTipoDePrato(TiposDePrato tipoSelecionado, String nomePrato) {
        if (tipoSelecionado.getPratos().stream().noneMatch(p -> p.getNomePrato().equalsIgnoreCase(nomePrato))) {
            tipoSelecionado.addPrato(new Prato(nomePrato));
        }
    }
}
