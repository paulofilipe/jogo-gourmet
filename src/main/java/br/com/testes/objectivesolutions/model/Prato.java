package br.com.testes.objectivesolutions.model;

public class Prato {
    private String nomePrato;

    public Prato(String nomePrato) {
        this.nomePrato = nomePrato;
    }

    public String getNomePrato() {
        return nomePrato;
    }

    public void setNomePrato(String nomePrato) {
        this.nomePrato = nomePrato;
    }
}
