package br.com.testes.objectivesolutions.model;

import java.util.ArrayList;
import java.util.List;

public class TiposDePrato {

    private List<TiposDePrato> subTipos;
    private String nomeTipo;
    private List<Prato> pratos;

    public TiposDePrato(List<TiposDePrato> subTipos, String nomeTipo, Prato pratoinicial) {
        this.subTipos = subTipos;
        this.nomeTipo = nomeTipo;
        pratos = new ArrayList<>();
        pratos.add(pratoinicial);
    }

    public List<TiposDePrato> getSubTipos() {
        return subTipos;
    }

    public String getNomeTipo() {
        return nomeTipo;
    }

    public void setNomeTipo(String nomeTipo) {
        this.nomeTipo = nomeTipo;
    }

    public List<Prato> getPratos() {
        return pratos;
    }

    public void addSubTipo(TiposDePrato tipo) {
        if (subTipos == null) {
            subTipos = new ArrayList<>();
        }

        this.subTipos.add(tipo);
    }

    public void addPrato(Prato prato) {
        if (pratos == null) {
            pratos = new ArrayList<>();
        }

        pratos.add(prato);
    }
}
