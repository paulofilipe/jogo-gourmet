package br.com.testes.objectivesolutions;

import br.com.testes.objectivesolutions.data.BaseDeConhecimentoRepository;
import br.com.testes.objectivesolutions.model.TiposDePrato;
import br.com.testes.objectivesolutions.ui.InterfaceJogo;

import java.util.List;
import java.util.function.Predicate;

public class Jogo {


    private static BaseDeConhecimentoRepository repository;
    private static InterfaceJogo ui;

    public static void main(String[] args) {

        repository = new BaseDeConhecimentoRepository();
        ui = new InterfaceJogo();

        do {
            ui.iniciaJogo();
            repository.criaChuteFinal();

            TiposDePrato tipoSelecionado = perguntaTipoPrato(null, repository.getBaseDeConhecimento());

            if (tentaAdivinharOPrato(tipoSelecionado)) {
                ui.mensagemAcerto();
                continue;
            }

            perguntaQualPratoECategoria(tipoSelecionado);

        } while (ui.perguntaSimOuNao("Continua?", "Quer continuar jogando?"));

        System.exit(0);
    }

    private static void perguntaQualPratoECategoria(TiposDePrato tipoSelecionado) {
        String nomePrato = ui.pedeUmValor("Desisto!!!", "Qual prato você pensou??");

        String nomeCategoria = ui.pedeUmValor("qual a categoria?", String.format(
                "O prato %s é _______ mas o prato %s não",
                nomePrato,
                repository.getPratoPalpite().getNomePrato()
        ));

        repository.adicionaOPratoABaseDeConhecimento(tipoSelecionado, nomePrato, nomeCategoria);
    }

    private static boolean tentaAdivinharOPrato(TiposDePrato tipoSelecionado) {
        repository.escolheUmPratoBaseadoNoTipo(tipoSelecionado);

        return ui.perguntaSimOuNao("Acertei?",
                String.format("O prato que você pensou é %s?", repository.getPratoPalpite().getNomePrato()));
    }

    private static TiposDePrato perguntaTipoPrato(TiposDePrato tipoBase, List<TiposDePrato> baseDeConhecimento) {
        TiposDePrato tipoEscolhido = baseDeConhecimento.stream().filter(escolhaTipo()).findFirst().orElse(null);

        if (tipoEscolhido == null) {
            return tipoBase;
        }

        if (tipoEscolhido.getSubTipos() == null || tipoEscolhido.getSubTipos().isEmpty()) {
            return tipoEscolhido;
        }

        return perguntaTipoPrato(tipoEscolhido, tipoEscolhido.getSubTipos());
    }

    private static Predicate<TiposDePrato> escolhaTipo() {
        return t -> ui.perguntaSimOuNao("Qual o tipo do prato?",
                String.format("O prato que você pensou é %s?", t.getNomeTipo()));
    }
}
