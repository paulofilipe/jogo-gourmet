package br.com.testes.objectivesolutions.ui;

import javax.swing.*;

public class InterfaceJogo {

    private JFrame frame;

    public InterfaceJogo() {
        frame = new JFrame();
    }

    public void iniciaJogo() {
        frame.setTitle("Jogo Gourmet");
//        frame.setVisible(true);

        JOptionPane.showMessageDialog(frame,
                "Escolha um prato que goste",
                "Jogo Gourmet",
                JOptionPane.PLAIN_MESSAGE);
    }

    public boolean perguntaSimOuNao(String titulo, String pergunta) {
        int i = JOptionPane.showConfirmDialog(
                frame,
                pergunta,
                titulo,
                JOptionPane.YES_NO_OPTION);

        return i == 0;
    }

    public String pedeUmValor(String titulo, String pergunta) {
        String valor = JOptionPane.showInputDialog(frame, pergunta, titulo, JOptionPane.PLAIN_MESSAGE);
        if (valor == null) {

            if (perguntaSimOuNao("Você não informou nenhum valor", "deseja encerrar o jogo?")) {
                System.exit(0);
            }

            return pedeUmValor(titulo, pergunta);
        }

        return valor;
    }

    public void mensagemAcerto() {
        JOptionPane.showMessageDialog(null, "Acertei novamente!!!");
    }
}
